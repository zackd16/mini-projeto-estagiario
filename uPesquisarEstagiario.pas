unit uPesquisarEstagiario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, dxGDIPlusClasses;

type
  TfmPesquisarEstagiario = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Image2: TImage;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    btPesquisar: TButton;
    edPesquisa: TEdit;
    Image1: TImage;
    mmPesquisa: TMemo;
    procedure btPesquisarClick(Sender: TObject);




  private
    { Private declarations }
  public
   lbNome: TLabel;
    lbTelefone: TLabel;
    lbSetor: TLabel;
    lbSalario: TLabel;
  end;

var
  fmPesquisarEstagiario: TfmPesquisarEstagiario;

implementation

uses
  uCriarEstagiario, uPrincipal,uListarEstagiario, uEstagiario;

{$R *.dfm}

procedure TfmPesquisarEstagiario.btPesquisarClick(Sender: TObject);
var
  wl_i: integer;
begin
    mmPesquisa.Text:='';

  for wl_i := 0 to length(wg_Estagiarios)-1 do
  begin
    if UpperCase(wg_Estagiarios[wl_i].Nome)= UpperCase(edPesquisa.Text) then
    begin
      mmPesquisa.Text := mmPesquisa.Text + #13 + #10 + wg_Estagiarios[wl_i].Nome+'                         '+wg_Estagiarios[wl_i].Telefone+'                    '+wg_Estagiarios[wl_i].Setor
      +'                       '+ FloatToStr(wg_Estagiarios[wl_i].Salario);
    end;
  end;

end;

end.
