unit uPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, dxGDIPlusClasses;

Var
  x : integer = 5;

  novoEstagiario: string;
  novoTelefone: string;
  novoSetor: string;
  novoSalario: string;


type
  TfmPrincipal = class(TForm)
    Panel1: TPanel;
    btNovoEstagiario: TButton;
    btVerSalario: TButton;
    btListarEstagiario: TButton;
    btPesquisar: TButton;
    Image1: TImage;
    procedure btNovoEstagiarioClick(Sender: TObject);
    procedure btListarEstagiarioClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btVerSalarioClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private

  public

  end;

var
  fmPrincipal: TfmPrincipal;


implementation


uses
  uCriarEstagiario, uListarEstagiario, uPesquisarEstagiario, uVerSalarios, uEstagiario;
{$R *.dfm}

procedure TfmPrincipal.btPesquisarClick(Sender: TObject);

begin
  fmPesquisarEstagiario:=TfmPesquisarEstagiario.Create(Self);
  Try
    fmPesquisarEstagiario.ShowModal;
  Finally
    fmPesquisarEstagiario.Free;
  end;
end;

procedure TfmPrincipal.btListarEstagiarioClick(Sender: TObject);
begin
  fmListarEstagiario:=TfmListarEstagiario.Create(Self);
  Try
    fmListarEstagiario.ShowModal;
  Finally
    fmListarEstagiario.Free;
  end;
end;

procedure TfmPrincipal.btNovoEstagiarioClick(Sender: TObject);
begin
  fmCriarEstagiario:=TfmCriarEstagiario.Create(Self);
  Try
    fmCriarEstagiario.ShowModal;
  Finally
    fmCriarEstagiario.Free;
  end;
end;

procedure TfmPrincipal.btVerSalarioClick(Sender: TObject);
begin
  fmVerSalarios:=TfmVerSalarios.Create(Self);
  Try
    fmVerSalarios.ShowModal;
  Finally
    fmVerSalarios.Free;
  end;
end;


procedure TfmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
var
  wl_i: integer;
begin
  for wl_i := 0 to length(wg_Estagiarios) - 1 do
    wg_Estagiarios[wl_i].Destroy;
end;

procedure TfmPrincipal.FormCreate(Sender: TObject);
begin
  AddEstagiario ('Renato','85964521853','Implantação',(400+Random(600)));
  AddEstagiario ('Gustavo','85963214586','Desenvolvimento',(400+Random(600)));
  AddEstagiario ('Renan','85991615425','Desenvolvimento',(400+Random(600)));
  AddEstagiario ('Luca','85964253689','Suporte',(400+Random(600)));
  AddEstagiario ('Isac','85974632598','Desenvolvimento',(400+Random(600)));
  AddEstagiario ('Daniel','85979859624','Desenvolvimento',(400+Random(600)));


end;

end.
