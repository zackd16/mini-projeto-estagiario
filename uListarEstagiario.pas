unit uListarEstagiario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, dxGDIPlusClasses;

type
  TfmListarEstagiario = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Image1: TImage;
    Label4: TLabel;
    Image2: TImage;
    Label5: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    btListar: TButton;
    mmListar: TMemo;

    procedure btListarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);

  private

  public


  end;

var
  fmListarEstagiario: TfmListarEstagiario;

implementation

uses
  uCriarEstagiario, uPrincipal, uEstagiario;

{$R *.dfm}



procedure TfmListarEstagiario.btListarClick(Sender: TObject);
var
  wl_i,wl_j: integer;
  wl_ordenar: TStringList;
begin
  if btListar.Tag = 1 then
    exit;
  btListar.Tag := 1;

  wl_ordenar := TStringList.Create;
  for wl_i := 0 to length(wg_Estagiarios)-1 do
    wl_ordenar.Add(wg_Estagiarios[wl_i].Nome);
    wl_ordenar.Sorted:= true;

  for wl_j := 0 to length(wg_Estagiarios) - 1 do
    for wl_i := 0 to length(wg_Estagiarios) - 1 do
      if UpperCase(wl_ordenar[wl_j])=UpperCase(wg_Estagiarios[wl_i].nome) then
      begin
        if wl_j =length(wg_Estagiarios) - 1 then
        else if uppercase(wl_ordenar[wl_j])=uppercase(wl_ordenar[wl_j+1]) then
          break;

        mmListar.Text := mmListar.Text + #13 + #10 + wg_Estagiarios[wl_i].Nome+'                       '+wg_Estagiarios[wl_i].Telefone+'                    '+wg_Estagiarios[wl_i].Setor+'                    '+ FloatToStr(wg_Estagiarios[wl_i].Salario);  
      end;
end;

procedure TfmListarEstagiario.FormActivate(Sender: TObject);
begin
  btListar.Tag := 0;
end;



end.
