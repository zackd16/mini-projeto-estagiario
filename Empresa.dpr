program Empresa;

uses
  Forms,
  uPrincipal in 'uPrincipal.pas' {fmPrincipal},
  uCriarEstagiario in 'uCriarEstagiario.pas' {fmCriarEstagiario},
  uListarEstagiario in 'uListarEstagiario.pas' {fmListarEstagiario},
  uPesquisarEstagiario in 'uPesquisarEstagiario.pas' {fmPesquisarEstagiario},
  uVerSalarios in 'uVerSalarios.pas' {fmVerSalarios},
  uEstagiario in 'uEstagiario.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmPrincipal, fmPrincipal);
  Application.Run;
end.
