object fmCriarEstagiario: TfmCriarEstagiario
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Criar Estagi'#225'rio'
  ClientHeight = 377
  ClientWidth = 521
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 521
    Height = 65
    Align = alTop
    Color = clActiveCaption
    ParentBackground = False
    TabOrder = 0
    object Label6: TLabel
      Left = 134
      Top = 16
      Width = 262
      Height = 41
      Caption = 'Novo Estagi'#225'rio'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -35
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 65
    Width = 521
    Height = 312
    Align = alClient
    Color = clSkyBlue
    ParentBackground = False
    TabOrder = 1
    object Label2: TLabel
      Left = 112
      Top = 63
      Width = 45
      Height = 19
      Caption = 'Nome'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 96
      Top = 103
      Width = 65
      Height = 19
      Caption = 'Telefone'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 116
      Top = 143
      Width = 41
      Height = 19
      Caption = 'Setor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edNome: TEdit
      Left = 168
      Top = 64
      Width = 220
      Height = 21
      TabOrder = 0
      OnKeyPress = edNomeKeyPress
    end
    object btConcluirCadastro: TButton
      Left = 184
      Top = 229
      Width = 121
      Height = 25
      Caption = 'Concluir'
      TabOrder = 3
      OnClick = btConcluirCadastroClick
    end
    object cbSetor: TComboBox
      Left = 168
      Top = 144
      Width = 153
      Height = 21
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 2
      Text = 'Desenvolvimento'
      Items.Strings = (
        'Desenvolvimento'
        'Implanta'#231#227'o'
        'Suporte')
    end
    object meTelefone: TMaskEdit
      Left = 167
      Top = 104
      Width = 94
      Height = 21
      EditMask = '!(00)00000-0000;0;'
      MaxLength = 14
      TabOrder = 1
    end
  end
end
