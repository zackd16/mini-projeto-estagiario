unit uCriarEstagiario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Mask;
var
  Add:integer=0;

type
  TfmCriarEstagiario = class(TForm)

    Panel1: TPanel;
    Label6: TLabel;
    Panel2: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    edNome: TEdit;
    btConcluirCadastro: TButton;
    cbSetor: TComboBox;
    meTelefone: TMaskEdit;
    procedure btConcluirCadastroClick(Sender: TObject);
    procedure edNomeKeyPress(Sender: TObject; var Key: Char);

    procedure edSalarioKeyPress(Sender: TObject; var Key: Char);


  private

  public

  end;

var
  fmCriarEstagiario: TfmCriarEstagiario;

procedure Cadastrados;

implementation

uses
  uPrincipal, uEstagiario;

{$R *.dfm}
procedure Cadastrados();
begin

end;

procedure TfmCriarEstagiario.btConcluirCadastroClick(Sender: TObject);
var
  wl_est: TEstagiario;
  wl_pos: Integer;
begin

  if edNome.Text='' then
  begin
    ShowMessage('Nome obrigatório');
    edNome.SetFocus;
    exit;
  end;

  if length(meTelefone.Text)<11 then
  begin
    ShowMessage('Telefone inválido');
    meTelefone.SetFocus;
    exit;

  end;
  AddEstagiaRio (edNome.Text,meTelefone.Text,cbSetor.Text,(400 +Random(600)));

  showMessage('Estagiário cadastrado!');
  fmCriarEstagiario.Close;
end;

procedure TfmCriarEstagiario.edNomeKeyPress(Sender: TObject; var Key: Char);
begin
  if ( Key In ['0'..'9']) then
    KEY := #0;
end;

procedure TfmCriarEstagiario.edSalarioKeyPress(Sender: TObject; var Key: Char);
begin
  if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
    key := #0;
end;

end.
