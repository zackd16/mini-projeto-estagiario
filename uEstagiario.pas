unit uEstagiario;


interface
uses
  SysUtils;

type
  TEstagiario = class(TObject)
  private
    FNome: String;
    FTelefone: String;
    FSetor: String;
    FSalario: real;
  public
    constructor create(p_nome,p_telefone,p_setor:string; p_salario:real);
    property Nome : String Read FNome Write FNome;
    property Telefone : String Read FTelefone Write FTelefone;
    property Setor : String Read FSetor Write FSetor;
    property Salario : real Read FSalario Write FSalario;
  end;

var
  wg_Estagiarios: array of TEstagiario;

  procedure AddEstagiario (p_nome, p_telefone, p_setor: string; p_salario: real);

implementation


{ TEstagiario }

constructor TEstagiario.create(p_nome, p_telefone, p_setor: string; p_salario: real);
begin
  inherited create();
  Nome:= p_nome;
  Telefone:= p_telefone;
  Setor:= p_setor;
  Salario:= p_salario;

end;


procedure AddEstagiaRio (p_nome, p_telefone, p_setor: string; p_salario: real);
var
  wl_est: TEstagiario;
  wl_pos: Integer;
begin
  wl_est := TEstagiario.create(p_nome, p_telefone, p_setor, p_salario);
  SetLength(wg_Estagiarios,Length(wg_Estagiarios)+1);
  wl_pos := High(wg_Estagiarios);
  wg_Estagiarios[wl_pos] := wl_est;

end;

end.
