object fmPrincipal: TfmPrincipal
  Left = 0
  Top = 0
  Caption = 'Naja - Programa'#231#227'o Orientada a Objetos'
  ClientHeight = 407
  ClientWidth = 588
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btCarro: TButton
    Left = 56
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Carro'
    TabOrder = 0
    OnClick = btCarroClick
  end
  object mmLog: TMemo
    Left = 56
    Top = 80
    Width = 497
    Height = 302
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object btMotos: TButton
    Left = 137
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Motos'
    TabOrder = 2
    OnClick = btMotosClick
  end
  object btBarco: TButton
    Left = 218
    Top = 32
    Width = 78
    Height = 25
    Caption = 'Barco'
    TabOrder = 3
    OnClick = btBarcoClick
  end
  object btJetski: TButton
    Left = 302
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Jet Ski'
    TabOrder = 4
    OnClick = btJetskiClick
  end
  object cbTriciclo: TCheckBox
    Left = 153
    Top = 63
    Width = 59
    Height = 17
    Caption = 'Triciclo'
    TabOrder = 5
  end
end
