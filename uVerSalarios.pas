unit uVerSalarios;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, dxGDIPlusClasses;

type
  TfmVerSalarios = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Image2: TImage;
    Label2: TLabel;
    Label5: TLabel;
    Image1: TImage;
    btCalcularMedia: TButton;
    mmSalario: TMemo;
    procedure btCalcularMediaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);


  private
    { Private declarations }
  public

  end;

var
  fmVerSalarios: TfmVerSalarios;

implementation

uses
  uCriarEstagiario, uPrincipal,uEstagiario;

{$R *.dfm}



procedure TfmVerSalarios.btCalcularMediaClick(Sender: TObject);
var
    wl_media: String;
    wl_soma: real;
    wl_count: real;
    wl_i: integer;

begin
  wl_soma:=0;
  wl_count:=0;

  for wl_i := 0 to length(wg_Estagiarios)-1 do
  begin
  wl_soma:=wl_soma+wg_Estagiarios[wl_i].Salario;
  wl_count:=wl_count+1;
  end;

  wl_media:= FloatToStr(wl_soma/wl_count);
  showMessage('A m�dia salarial � '+ FormatFloat('R$ ###,##0.00',StrToFLoat(wl_media)));

end;

procedure TfmVerSalarios.FormActivate(Sender: TObject);
var
  wl_i,wl_j: integer;
  wl_ordenar: TStringList;
begin
  wl_ordenar := TStringList.Create;
  for wl_i := 0 to length(wg_Estagiarios)-1 do
    wl_ordenar.Add(wg_Estagiarios[wl_i].Nome);
    wl_ordenar.Sorted:= true;

  for wl_j := 0 to length(wg_Estagiarios) - 1 do
    for wl_i := 0 to length(wg_Estagiarios) - 1 do
      if UpperCase(wl_ordenar[wl_j])=UpperCase(wg_Estagiarios[wl_i].nome) then
      begin
        if wl_j =length(wg_Estagiarios) - 1 then
        else if uppercase(wl_ordenar[wl_j])=uppercase(wl_ordenar[wl_j+1]) then
          break;

        mmSalario.Text := mmSalario.Text + #13 + #10 + wg_Estagiarios[wl_i].Nome+'                                                        '+ FloatToStr(wg_Estagiarios[wl_i].Salario);
      end;
end;
end.
